@if(isset($slides) && count($slides) > 0)
	<div class="">
		<div class="master-slider ms-skin-default" id="">
			@foreach($slides as $slide)
			    <div class="ms-slide">
		        	<img src="{{ route('getfile', [$slide->arquivo_id, 'grande']) }}" 
						 class="img-responsive" 
						 width  = "{{ config('imagens.slides.imagem.grande.0') }}"
						 height = "{{ config('imagens.slides.imagem.grande.1') }}" 
						 alt = "{{ $quem_somos->arquivo->alt or '' }}" />
		        	@if(!empty($slide->url))
			        	<a href="{{ $slide->url }}" target="_blank">{{ $slide->nome }}</a>
			        @endif

			        @if(!empty($slide->texto1))
			        	<div class="ms-layer ms-caption" data-offset-y="0" data-origin= "mc" data-type="text" data-effect="bottom(90)" data-duration="800" data-ease="easeOutQuart">
			        		@if(isset($slide->texto1))
			        			<p>{{ $slide->texto1 }}</p>
			        		@endif
			        		@if(isset($slide->texto2))
			        			<p>{{ $slide->texto2 }}</p>
			        		@endif
		        		</div>
			        @endif
			    </div>
		    @endforeach
		</div>
	</div>
@endif