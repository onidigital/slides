@extends('layouts.admin')

@section('content')
@if(!isset($registro->id))
    {!! Form::open(['url' => $caminho, 'files'=>true ]) !!}
@else
    {!! Form::model($registro, ['url' => $caminho.$registro->id, 'method'=>'put', 'files'=>true]) !!}
@endif
    {!! Form::hidden('id', null) !!}
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="header">
                <h4 class="title">{{ $registro->nome or $titulo }} <span class="pull-right">{!! $html_toggle or '' !!}</span></h4>
            </div>
            <div class="content">
                    <div class="form-group" >
                        <div class="row">
                            <div class="col-sm-6" >
                                {!! Form::label('nome', 'Nome:') !!}
                                {!! Form::text('nome', null, ['class' => 'form-control', 'autofocus required'] ) !!}
                            </div>
                            <div class="col-sm-6" >
                                {!! Form::label('url', 'URL:') !!}
                                <div class="input-group">
                                    <span class="input-group-addon" data-toggle="tooltip" title="Insira uma url externa ou um id de uma parte do site para rolar"><i class="ion-ios-information-outline"></i></span>
                                    {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'http://www.outrosite.com.br ou #id-do-elemento'] ) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" >
                        <div class="row">
                            <div class="col-sm-6" >
                                {!! Form::label('texto1', 'Frase principal:') !!}
                                {!! Form::text('texto1', null, ['class' => 'form-control'] ) !!}
                            </div>
                            <div class="col-sm-6" >
                                {!! Form::label('texto2', 'Frase secundária:') !!}
                                {!! Form::text('texto2', null, ['class' => 'form-control'] ) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group" >
                        {!! Form::label('peso', 'Peso:') !!}
                        {!! Form::number('peso', null, ['class' => 'form-control'] ) !!}
                    </div>

                    <div class="form-group" >
                        {!! Form::label('imagem', 'Imagem:') !!}
                        {!! Form::file('imagem') !!}
                    </div>

                    @if( (isset($registro->arquivo_id)) && $registro->arquivo_id > 0)
                    <div class="form-group" >
                        <img src="{{route('getfile', array($registro->arquivo_id, 'grande') )}}" alt="{{ $registro->arquivo->alt }}" class="img-responsive" />
                        <label><input type="checkbox" name="remover_arquivo[]" value="arquivo_id" > Remover arquivo</label>
                    </div>
                    @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                @include('admin._partes._botao_voltar')
                {!! Form::submit('Gravar', ['class' => 'btn btn-fill btn-wd btn-success pull-right']) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
