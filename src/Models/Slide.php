<?php

namespace Onicmspack\Slides\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    //
    protected $fillable = ['nome',
    					   'arquivo_id',
    					   'texto1',
    					   'texto2',
    					   'url',
    					   'peso',
    					   'status'];
    // Este array será usado caso não haja a configuração em config/imagens.php para esta entidade
    public $formatos = [
    	'arquivo_id' =>[
    		'grande' => [1366,700],
    	]
    ];

    // Para retornar o arquivo do slide:
    public function arquivo()
    {
        return $this->belongsTo('Onicmspack\Arquivos\Models\Arquivo', 'arquivo_id');
    }
}
