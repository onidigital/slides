<?php

Route::group(['middleware' => ['web','auth', 'authorization'], 'prefix' => 'admin' ], function () {
	// Menu do slide:
	Route::resource('slides', 'Onicmspack\Slides\SlidesController');
	Route::post('slides/{id}/atualizar_status','Onicmspack\Slides\SlidesController@atualizar_status'); // Atualizar Status Ajax
});