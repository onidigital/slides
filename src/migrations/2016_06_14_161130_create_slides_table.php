<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('arquivo_id')->nullable()->unsigned();
            $table->foreign('arquivo_id')->references('id')->on('arquivos');
            $table->string('texto1')->nullable();
            $table->string('texto2')->nullable();
            $table->string('url')->nullable();
            $table->string('peso')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slides');
    }
}
