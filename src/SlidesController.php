<?php 
namespace Onicmspack\Slides;

use Onicmspack\Slides\Models\Slide;
use Onicmspack\Arquivos\Models\Arquivo as Arquivo;
use Onicmspack\Slides\Requests\SlidesRequest as SlidesRequest;
use Illuminate\Routing\Controller as Controller;
 
class SlidesController extends Controller
{
 	public $caminho = 'admin/slides/';
    public $views   = 'admin/vendor/slides/';
    public $titulo  = 'Slides';

    public function index()
    {
        $registros = Slide::with('arquivo')->get();
        $registros = configurar_status_toogle($registros, $this->caminho);
        return view($this->views.'.index',['registros'=>$registros],[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function create()
    {
        $html_toggle = gerar_status_toggle( array('status' => 1) );
        return view($this->views.'.form',[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
                    'html_toggle' => $html_toggle,
               ]);
    }

    public function store(SlidesRequest $request)
    {
        $input = $request->all();
        if(!isset($input['status']))
            $input['status'] = 0;
        $input['arquivo_id'] = $this->upload($input);
        Slide::create($input);

        $request->session()->flash('alert-success', config('mensagens.registro_inserido'));
        return redirect($this->caminho.'create');
    }

    public function show($id)
    {
        $registro = Slide::find($id);
        $html_toggle = gerar_status_toggle( $registro );
        return view($this->views.'.form', compact('registro'),[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
                    'html_toggle' => $html_toggle,
               ]);
    }

    public function update(SlidesRequest $request, $id)
    {
        $input = $request->all();
        if(!isset($input['status']))
            $input['status'] = 0;

        if(isset($input['remover_arquivo'])){
            foreach($input['remover_arquivo'] as $arq)
                $input[$arq] = null;
        }

        // se mudou o arquivo:
        if(isset($input['imagem']))
            $input['arquivo_id'] = $this->upload($input);

        $update = Slide::find($id)->update($input);

        $request->session()->flash('alert-success', config('mensagens.registro_alterado'));
        return redirect($this->caminho.$id.'');
    }

    public function destroy($id)
    {
        Slide::find($id)->delete();
        return redirect($this->caminho);
    }

    public function upload($input)
    {
        if(!empty($input['imagem'])){
            $manipulador = new Arquivo;
            $file = $manipulador->add($input['imagem']);
            // recortar?
            $manipulador->recortar('slides', 'arquivo_id', $file->id);
            return $file->id;
        }
        return false;
    }

    // Atualiza um campo boolean de um registro via ajax
    public function atualizar_status($id, $coluna = 'status')
    {
        // Verifica o status atual e dá um update com o novo status:
        $registro = Slide::find($id);
        // Se encontrou o registro:
        if(isset($registro->{$coluna})){
            $novo = !$registro->{$coluna};
            $update = Slide::find($id)->update( array( $coluna =>$novo ) );
            $resposta['success'] = 'success';
            $resposta['status']  = '200';
        }else{
            $resposta['success'] = 'fail';
            $resposta['status']  = '0';
        }
        return \Response::json($resposta);
    }
 
}



